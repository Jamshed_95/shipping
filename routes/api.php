<?php

use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Api\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', [UserController::class, 'register']);
Route::post('login', [UserController::class, 'login']);

/***/
Route::get('getStatus', [ApiController::class, 'getStatus']);
Route::post('createOrder', [ApiController::class, 'createOrder']);
/***/

Route::middleware('auth:api')->group(function () {
    Route::post('logout', [UserController::class, 'logout']);

    Route::post('addDriver', [ApiController::class, 'addDriver']);
    Route::post('editDriver', [ApiController::class, 'editDriver']);
    Route::post('deleteDriver', [ApiController::class, 'deleteDriver']);
    Route::post('addDriverCar', [ApiController::class, 'addDriverCar']);
    Route::get('getAllDrivers', [ApiController::class, 'getDrivers']);
    Route::get('getDriver/{driver}', [ApiController::class, 'getDriver']);

    Route::post('addTransport', [ApiController::class, 'addTransport']);
    Route::post('editTransport', [ApiController::class, 'editTransport']);
    Route::post('deleteTransport', [ApiController::class, 'deleteTransport']);
    Route::post('addTransportRate', [ApiController::class, 'addTransportRate']);
    Route::get('getCarBrand', [ApiController::class, 'getCarBrand']);
    Route::get('getTransport', [ApiController::class, 'getTransport']);
    Route::get('getBrandModel/{carBrand}', [ApiController::class, 'getBrandModel']);

    Route::post('addRate', [ApiController::class, 'addRate']);
    Route::post('editRate', [ApiController::class, 'editRate']);
    Route::post('deleteRate', [ApiController::class, 'deleteRate']);
    Route::get('getRate', [ApiController::class, 'getRate']);

    Route::get('getOrders', [ApiController::class, 'getOrders']);

});
