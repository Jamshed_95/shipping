<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->integer('rate_id');
            $table->string('from_to_address');
            $table->index('from_to_address');
            $table->string('from_coord');
            $table->string('to_coord');
            $table->index('from_coord');
            $table->index('to_coord');
            $table->decimal('minimum_cost');
            $table->decimal('cost_km_calc');
            $table->decimal('cost_minutes_calc');
            $table->decimal('total_cost');
            $table->integer('status_id');
            $table->index('status_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
