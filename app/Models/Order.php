<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    public function status(){
        $this->belongsTo(Status::class,'status_id','id');
    }

    public function fromAdress(){
        $this->belongsTo(Address::class,'from_address','id');
    }

    public function toAddress(){
        $this->belongsTo(Address::class,'to_address','id');
    }
}
