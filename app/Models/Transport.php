<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transport extends Model
{
    use HasFactory;

    public function brand(){
        $this->belongsTo(CarBrand::class,'brand_id','id');
    }

    public function modelId(){
        $this->belongsTo(BrandModel::class,'model_id','id');
    }
}
