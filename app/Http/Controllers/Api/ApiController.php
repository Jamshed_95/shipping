<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\CarBrand;
use App\Models\Driver;
use App\Models\Order;
use App\Models\Rate;
use App\Models\Status;
use App\Models\Transport;
use App\Models\TransportRate;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Lcobucci\JWT\Exception;

class ApiController extends Controller
{

    public function createOrder(Request $request)
    {

        $this->validate($request, [
            'from_address' => 'required',
            'to_address' => 'required',
            'from_coord' => 'required',
            'to_coord' => 'required',
            'rate_id' => 'required'
        ]);
        try {
            $client = new Client();
            $coords = $request->from_coord . ';' . $request->to_coord;
            $response = $client->request('GET', 'https://routing.openstreetmap.de/routed-car/' . $coords . '?overview=false', [
                'headers' => [
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json'
                ],
            ]);
            $result = json_decode($response->getBody());
            $route = $result->routes[0];
            $rate = Rate::find($request->rate_id);
            if ($rate) {//вдруг отпрвили null
                $cost_km_calc = (($route->distance / 1000) - $rate->free_km_count) * $rate->km_cost;
                $cost_minutes_calc = (($route->duration / 60) - $rate->free_minutes) * $rate->minutes_cost;
                $total = $rate->minimum_cost + $cost_minutes_calc + $cost_km_calc;
                $order = new Order();
                $order->from_address = $request->from_address;
                $order->to_address = $request->from_address;
                $order->from_coord = $request->from_coord;
                $order->to_coord = $request->to_coord;
                $order->rate_id = $rate->id;
                $order->minimum_cost = $rate->minimum_cost;
                $order->cost_km_calc = $cost_km_calc;
                $order->cost_minutes_calc = $cost_minutes_calc;
                $order->total_cost = $total;
                $order->status_id = 1;
                $order->save();
            }
            return response()->json(['code' => 200, 'data' => $result->routes[0]], 200);
        } catch (\Exception $ex) {
            return response()->json(['code' => 401, 'data' => 'Error'], 104);
        }

    }

    public function getStatus(){
        return  response()->json(['code' => 200, 'data' => Status::all()], 200);
    }

    /*DRIVER*/
    public function addDriver(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:4',
            'birth_date' => 'required|date',
            'driver_license_number' => 'required',
            'driver_license_term' => 'required|date',
        ]);

        try {
            $driver = new Driver();
            $driver->name = $request->name;
            $driver->name = $request->birth_date;
            $driver->name = $request->driver_license_number;
            $driver->name = $request->driver_license_term;
            $driver->save();
            return response()->json(['code' => 200, 'data' => $driver], 200);
        } catch (\Exception $ex) {
            return response()->json(['code' => 401, 'data' => $ex], 401);
        }
    }

    public function editDriver(Request $request)
    {

        $this->validate($request, [
            'driver_id' => 'required',
            'name' => 'required|min:4',
            'birth_date' => 'required|date',
            'driver_license_number' => 'required',
            'driver_license_term' => 'required|date',
        ]);

        try {
            $driver = Driver::findOrFail($request->driver_id);
            $driver->name = $request->name;
            $driver->name = $request->birth_date;
            $driver->name = $request->driver_license_number;
            $driver->name = $request->driver_license_term;
            $driver->save();
            return response()->json(['code' => 200, 'data' => $driver], 200);
        } catch (\Exception $ex) {
            return response()->json(['code' => 401, 'data' => $ex], 401);
        }

    }

    public function deleteDriver(Request $request)
    {
        $this->validate($request, [
            'driver_id' => 'required'
        ]);
        try {
            Driver::findOrFail($request->driver_id)->delete();
            return response()->json(['code' => 200, 'data' => 'driver deleted successfuly'], 200);
        } catch (\Exception $ex) {
            return response()->json(['code' => 401, 'data' => $ex], 401);
        }
    }

    public function getDrivers()
    {
        $drivers = Driver::all();
        return response()->json(['code' => 200, 'data' => $drivers], 200);
    }

    public function getDriver(Driver $driver)
    {
        return response()->json(['code' => 200, 'data' => $driver], 200);
    }

    public function addDriverCar(Request $request)
    {
        $this->validate($request, [
            'driver_id' => 'required',
            'transport_id' => 'required'
        ]);
        try {
            Driver::find($request->driver_id)
                ->update(['transport_id' => $request->transport_id]);
            return response()->json(['code' => 200, 'data' => 'success'], 200);
        } catch (\Exception $ex) {
            return response()->json(['code' => 401, 'data' => $ex], 401);
        }

    }
    /*END DRIVER*/

    /*Transport*/
    public function getCarBrand()
    {
        $brand = CarBrand::all();
        return response()->json(['code' => 200, 'data' => $brand], 200);
    }

    public function getBrandModel(CarBrand $carBrand)
    {
        return response()->json(['code' => 200, 'data' => $carBrand], 200);
    }

    public function addTransport(Request $request)
    {
        $this->validate($request, [
            'brand_id' => 'required',
            'model_id' => 'required',
            'number' => 'required',
            'color' => 'required',
            'year_of_issue' => 'required',
        ]);

        try {
            $transport = new Transport();
            $transport->brand_id = $request->brand_id;
            $transport->model_id = $request->model_id;
            $transport->number = $request->number;
            $transport->color = $request->color;
            $transport->year_of_issue = $request->year_of_issue;
            $transport->save();
            return response()->json(['code' => 200, 'data' => $transport], 200);
        } catch (\Exception $ex) {
            return response()->json(['code' => 401, 'data' => $ex], 401);
        }

    }

    public function editTransport(Request $request)
    {
        $this->validate($request, [
            'transport_id' => 'required',
            'brand_id' => 'required',
            'model_id' => 'required',
            'number' => 'required',
            'color' => 'required',
            'year_of_issue' => 'required',
        ]);

        try {
            $transport = Transport::find($request->transport_id);
            $transport->brand_id = $request->brand_id;
            $transport->number = $request->number;
            $transport->color = $request->color;
            $transport->year_of_issue = $request->year_of_issue;
            $transport->save();
            return response()->json(['code' => 200, 'data' => $transport], 200);
        } catch (\Exception $ex) {
            return response()->json(['code' => 401, 'data' => $ex], 401);
        }

    }

    public function deleteTransport(Request $request)
    {
        $this->validate($request, [
            'transport_id' => 'required'
        ]);
        try {
            Transport::findOrFail($request->transport_id)->delete();
            return response()->json(['code' => 200, 'data' => 'Transport deleted successfuly'], 200);
        } catch (\Exception $ex) {
            return response()->json(['code' => 401, 'data' => $ex], 401);
        }
    }

    public function addTransportRate(Request $request)
    {
        $this->validate($request, [
            'transport_id' => 'required',
            'rate_id' => 'required'
        ]);
        try {
            $transport = new TransportRate();
            $transport->transport_id = $request->transport_id;
            $transport->rate_id = $request->rate_id;
            $transport->save();
            return response()->json(['code' => 200, 'data' => $transport], 200);
        } catch (\Exception $ex) {
            return response()->json(['code' => 401, 'data' => $ex], 401);
        }
    }

    public function getTransport(){
        return response()->json(['code' => 200, 'data' => Transport::with('brand','modelId')], 200);
    }
    /*End Transport*/

    /*Rate*/
    public function addRate(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'minimum_cost' => 'required',
            'km_cost' => 'required',
            'minutes_cost' => 'required',
            'free_km_count' => 'required',
            'free_minutes' => 'required'
        ]);

        try {
            $rate = new Rate();
            $rate->name = $request->name;
            $rate->minimum_cost = $request->minimum_cost;
            $rate->km_cost = $request->km_cost;
            $rate->minutes_cost = $request->minutes_cost;
            $rate->free_km_count = $request->free_km_count;
            $rate->free_minutes = $request->free_minutes;
            $rate->save();
            return response()->json(['code' => 200, 'data' => $rate], 200);
        } catch (\Exception $ex) {
            return response()->json(['code' => 401, 'data' => $ex], 401);
        }

    }

    public function editRate(Request $request)
    {
        $this->validate($request, [
            'rate_id' => 'required',
            'name' => 'required',
            'minimum_cost' => 'required',
            'km_cost' => 'required',
            'minutes_cost' => 'required',
            'free_km_count' => 'required',
            'free_minutes' => 'required'
        ]);

        try {
            $rate = Rate::find($request->rate_id);
            $rate->name = $request->name;
            $rate->minimum_cost = $request->minimum_cost;
            $rate->km_cost = $request->km_cost;
            $rate->minutes_cost = $request->minutes_cost;
            $rate->free_km_count = $request->free_km_count;
            $rate->free_minutes = $request->free_minutes;
            $rate->save();
            return response()->json(['code' => 200, 'data' => $rate], 200);
        } catch (\Exception $ex) {
            return response()->json(['code' => 401, 'data' => $ex], 401);
        }

    }

    public function deleteRate(Request $request)
    {
        $this->validate($request, [
            'rate_id' => 'required'
        ]);
        try {
            Rate::findOrFail($request->rate_id)->delete();
            return response()->json(['code' => 200, 'data' => 'Rate deleted successfuly'], 200);
        } catch (\Exception $ex) {
            return response()->json(['code' => 401, 'data' => $ex], 401);
        }

    }

    public function getRate()
    {
        $rate = Rate::all();
        return response()->json(['code' => 200, 'data' => $rate], 200);
    }

    /*End Rate*/

    public function getOrders()
    {
        $orders = Order::all();
        return response()->json(['code' => 200, 'data' => $orders], 200);
    }
}
