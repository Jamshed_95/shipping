<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function register(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'login' => 'required',
            'password' => 'required|min:6',
        ]);
        $check = User::where('login',$request->login)->first();
        if ($check){
            return response()->json(['code' => 401,'data' => 'this login exist'], 401);
        }
        $user = User::create([
            'name' => $request->name,
            'login' => $request->login,
            'password' => Hash::make($request->password)
        ]);
        $token = $user->createToken('jm')->accessToken;
        return response()->json(['code' => 200,'token' => $token], 200);
    }

    public function login(Request $request)
    {
        $data = [
            'login' => $request->login,
            'password' => $request->password
        ];

        if (auth()->attempt($data)) {
            $token = auth()->user()->createToken('jm')->accessToken;
            return response()->json(['token' => $token ,'code'=>200], 200);
        } else {
            return response()->json(['code' => 401,'data' => 'Unauth'], 401);
        }
    }

    public function logout(Request $request){
        $accessToken = Auth::user()->token();
        $token= $request->user()->tokens->find($accessToken);
        $token->revoke();
        $response=array();
        $response['code']=200;
        $response['data']="Successfully logout";
        return response()->json($response)->header('Content-Type', 'application/json');
    }
}
