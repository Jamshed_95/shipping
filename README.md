## Инструкция

1. Настроит .env файл, соединение с база данных
2. Запустить команды:
  - a).composer update (или composer install);
  - b).php artisan key:generate;
  - c).php artisan migrate;
  - d).php artisan passport:install
  - e).php artisan route:clear
  - f).php artisan config:clear


## GOOD LUCK
